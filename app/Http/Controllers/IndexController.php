<?php


namespace App\Http\Controllers;


use App\Models\Airport;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class IndexController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function register()
    {
        return view('auth.register');
    }

    /**
     * @return Application|Factory|View
     */
    public function login()
    {
        return view('auth.login');
    }

    /**
     * @return Application|Factory|View
     */
    public function search()
    {
        return view('index', [
            'airports' => Airport::all()
        ]);
    }
}
