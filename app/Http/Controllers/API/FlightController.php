<?php

namespace App\Http\Controllers\API;

use App\Models\Airport;
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Models\Flight;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\JsonResponse;
use App\Http\Helpers\FlightHelper;

class FlightController extends BaseController
{
    /**
     * @var FlightHelper $helper
     */
    private $helper;

    /**
     * FlightController constructor.
     * @param FlightHelper $helper
     */
    public function __construct(FlightHelper $helper)
    {
        $this->helper = $helper;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function search(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'departure_airport' => 'required|exists:App\Models\Airport,code',
            'arrival_airport' => 'required|exists:App\Models\Airport,code',
            'departure_date' => 'required|date'
        ]);

        if ($validator->fails()) {
            return $this->sendError($validator->errors()->first());
        }

        $data = $request->all();
        $departureAirport = Airport::where('code', $data['departure_airport'])->first();
        $arrivalAirport = Airport::where('code', $data['arrival_airport'])->first();

        $dateFrom = $data['departure_date'];
        $dateTo = $this->helper->getDateTo($dateFrom);

        $flights = Flight::where('departure_airport_id', $departureAirport->id)
            ->where('arrival_airport_id', $arrivalAirport->id)
            ->where('departure_at', '>=', $dateFrom)
            ->where('departure_at', '<', $dateTo)
            ->orderBy('departure_at', 'ASC')
            ->get();

        if (!$flights->count()) {
            return $this->sendError('Flights not found.');
        }

        $result = $this->helper->getPackedData($flights);

        return $this->sendResponse($result, 'Flights searched successfully.');
    }
}
