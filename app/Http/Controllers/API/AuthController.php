<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\JsonResponse;

class AuthController extends BaseController
{
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:App\Models\User,email',
            'password' => 'required',
            'confirm_password' => 'required|same:password',
        ]);

        if ($validator->fails()) {
            return $this->sendError($validator->errors()->first());
        }

        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        User::create($input);
        return $this->sendResponse(true, 'User register successfully.');
    }

    /**
     * @param Request $request
     * @return JsonResponse|Request
     */
    public function login(Request $request)
    {
        $login = $request->validate([
            'email' => 'required|string',
            'password' => 'required|string',
        ]);

        if (!Auth::attempt($login, true)) {
            return $this->sendError('Invalid login credentials.');
        }

        $accessToken = Auth::user()->createToken('authToken')->accessToken;

        return response()->json(['success' => true, 'access_token' => $accessToken]);
    }
}
