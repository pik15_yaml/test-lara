<?php


namespace App\Http\Helpers;


use Illuminate\Support\Collection;

class FlightHelper
{
    /**
     * @param string $date
     * @return string
     * @throws \Exception
     */
    public function getDateTo(string $date)
    {
        $dateTo = new \DateTime($date);
        $dateTo->add(new \DateInterval('P1D')); // P1D means a period of 1 day
        return $dateTo->format('Y-m-d');
    }

    /**
     * @param Collection $flights
     * @return array
     */
    public function getPackedData(Collection $flights)
    {
        $result = [];
        foreach ($flights as $key => $flight) {
            $result[$key]['transporter']['code'] = $flight->transporter->code;
            $result[$key]['transporter']['name'] = $flight->transporter->name;
            $result[$key]['flightNumber'] = $flight->number;
            $result[$key]['departureAirport'] = $flight->departureAirport->code;
            $result[$key]['arrivalAirport'] = $flight->arrivalAirport->code;
            $result[$key]['departureDateTime'] = $flight->departure_at;
            $result[$key]['arrivalDateTime'] = $flight->arrival_at;
            $result[$key]['duration'] = $flight->duration;
        }
        return $result;
    }
}
