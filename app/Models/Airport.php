<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Airport
 * @package App\Models
 * @property int $id
 * @property string $name
 * @property string $code
 *
 */
class Airport extends Model
{

}
