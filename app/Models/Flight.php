<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Transporter
 * @package App\Models
 * @property int $id
 * @property int $departure_airport_id
 * @property int $arrival_airport_id
 * @property int $transporter_id
 * @property float duration
 * @property string $number
 * @property \DateTime $departure_at
 * @property \DateTime $arrival_at
 *
 */
class Flight extends Model
{
    public function departureAirport()
    {
        return $this->belongsTo(Airport::class, 'departure_airport_id', 'id');
    }

    public function arrivalAirport()
    {
        return $this->belongsTo(Airport::class, 'arrival_airport_id', 'id');
    }

    public function transporter()
    {
        return $this->belongsTo(Transporter::class, 'transporter_id', 'id');
    }
}
