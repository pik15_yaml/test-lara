<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Transporter
 * @package App\Models
 * @property int $id
 * @property string $name
 * @property string $code
 *
 */
class Transporter extends Model
{

}
