<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/register', 'App\Http\Controllers\IndexController@register')->name('showRegisterForm');
Route::get('/login', 'App\Http\Controllers\IndexController@login')->name('showLoginForm');
Route::get('/search', 'App\Http\Controllers\IndexController@search')->name('showSearchForm');
Route::get('/', 'App\Http\Controllers\IndexController@login');

/*Route::middleware('auth:api')->group( function () {
    Route::get('/search', 'App\Http\Controllers\IndexController@search')->name('showSearchForm');
});

