@extends('layout.layout')

@section('content')
    <div id="result"></div>
    <div class="container block">
        <form id="search-form">
            <div class="row">
                <label for="">Аэропорт вылета</label>
                <select class="form-control" name="departure_airport" id="">
                    @foreach ($airports as $airport)
                        <option value="{{ $airport->code }}">{{ $airport->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="row">
                <label for="">Аэропорт назначения</label>
                <select class="form-control" name="arrival_airport" id="">
                    @foreach ($airports as $airport)
                        <option value="{{ $airport->code }}">{{ $airport->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="row">
                <label for="">Дата перелета</label>
                <input name="departure_date" type="date" class="form-control">
            </div>
            <div class="btn-container row">
                <input type="submit" value="Поиск" class="btn btn-success">
            </div>
        </form>
    </div>

    <table class="table" style="display: none">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Имя перевозчика</th>
            <th scope="col">Код перевозчика</th>
            <th scope="col">Номер рейса</th>
            <th scope="col">Аэропорт вылета</th>
            <th scope="col">Аэропорт назначения</th>
            <th scope="col">Время вылета</th>
            <th scope="col">Время прилета</th>
            <th scope="col">Длительность</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
@endsection

@push('scripts')
    <script>
        $("#search-form").submit(function(event) {
            let data = $(this).serializeArray().reduce(function(obj, item) {
                obj[item.name] = item.value;
                return obj;
            }, {});

            let token = localStorage.getItem('token');
            let resultBlock = $('#result');
            resultBlock.empty();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}",
                    'Accept': 'application/json',
                    'Authorization': "Bearer " + token
                }
            });

            $('.table tbody').empty();

            $.ajax({
                type: "GET",
                url: "/api/search",
                data: data,
            }).done(function (response) {
                if (response["success"] == true) {
                    $('.table').show();
                    showData(response['data']);
                } else {
                    resultBlock.html("<div class=\"alert alert-danger\">" + response['message'] + '</div>');
                }
            }).fail(function () {
                resultBlock.html("<div class=\"alert alert-danger\">Fail</div>");
            });

            event.preventDefault();

            return false;
        });

        function showData(data)
        {
            for (let i = 0; i < data.length; i++) {
                let str = '<tr><td>' + (i+1) + '</td>';
                let transporter = data[i].transporter;
                str += '<td>' + transporter['name'] + '</td>';
                str += '<td>' + transporter['code'] + '</td>';
                $.each(data[i], function(index, value) {
                    if (typeof value === 'object') {
                        return true;
                    }
                    str += '<td>' + value + '</td>';
                });
                str += '</tr>';
                $('.table tbody').append(str);
            }
        }
    </script>
@endpush
