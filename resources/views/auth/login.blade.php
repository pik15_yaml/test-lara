@extends('layout.layout')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <!-- Authentication card start -->
                <div class="login-card card-block auth-body mr-auto ml-auto">
                    <div id="result"></div>
                    <form id="login-form" class="md-float-material">
                        <div class="auth-box">
                            <div class="row m-b-20">
                                <div class="col-md-12">
                                    <h3 class="text-left txt-primary"> {{ __('Login') }}</h3>
                                </div>
                            </div>
                            <hr/>

                            <input id="email" type="email" placeholder="Email" class="form-control" name="email" style="margin-bottom: 10px" required autocomplete="email" autofocus>
                            <span class="md-line"></span>

                            <input id="password" type="password" placeholder="{{ __('Password') }}" class="form-control" name="password" required autocomplete="email" autofocus>
                            <span class="md-line"></span>

                            <div class="row m-t-30">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary btn-md btn-block waves-effect text-center m-b-20"> {{ __('Login') }}</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- end of form -->
                </div>
                <!-- Authentication card end -->
            </div>
            <!-- end of col-sm-12 -->
        </div>
        <!-- end of row -->
    </div>
@endsection

@push('scripts')
    <script>
        $("#login-form").submit(function(event) {
            let data = $(this).serializeArray().reduce(function(obj, item) {
                obj[item.name] = item.value;
                return obj;
            }, {});

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                }
            });

            let resultBlock = $('#result')

            $.ajax({
                type: "POST",
                url: "{{ route('login') }}",
                data: data,
            }).done(function (response) {
                if (response["success"] == true) {
                    localStorage.setItem('token', response['access_token']);
                    window.location.href = '/search';
                } else {
                    resultBlock.html("<div class=\"alert alert-danger\">" + response['message'] + '</div>');
                }
            }).fail(function () {
                resultBlock.html("<div class=\"alert alert-danger\">Fail</div>");
            });

            event.preventDefault();

            return false;
        });
    </script>
@endpush
